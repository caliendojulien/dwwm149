// let livre = {
//     titre: 'Le tour du monde en 80 jours',
//     auteur: 'Jules Verne',
//     annee: 1873
// }
//
// document.getElementById('livre1').innerText = livre.titre;

import {Livre} from "./Livre.js";

let livre = new Livre(
    'Le tour du monde en 80 jours',
    'Jules Verne',
    1873
);
document.getElementById('livre1').innerText = livre.titre;