export class Livre {
    #titre;
    #auteur;
    #annee;

    constructor(titre, auteur, annee) {
        this.#titre = titre;
        this.#auteur = auteur;
        this.#annee = annee;
    }

    get titre() {
        return this.#titre;
    }

    set titre(value) {
        this.#titre = value;
    }

    get auteur() {
        return this.#auteur;
    }

    set auteur(value) {
        this.#auteur = value;
    }

    get annee() {
        return this.#annee;
    }

    set annee(value) {
        this.#annee = value;
    }
}