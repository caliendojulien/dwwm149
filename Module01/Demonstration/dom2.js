document.getElementById('bSubmit').addEventListener(
    'click',
    () => {
        // Créer un nouvel element
        let nouvelElement = document.createElement('li');
        nouvelElement.innerText = document.getElementById('iTodo').value;
        // Attacher le nouvel element au DOM
        document.getElementById('todos').appendChild(nouvelElement);
    }
);