"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SuperHero = void 0;
var SuperHero = /** @class */ (function () {
    function SuperHero(nom, slipAuDessusDeSesVetements, cape, force) {
        if (cape === void 0) { cape = true; }
        this._nom = nom;
        this._slipAuDessusDeSesVetements = slipAuDessusDeSesVetements;
        this._cape = cape;
        this._force = force;
    }
    Object.defineProperty(SuperHero.prototype, "nom", {
        get: function () {
            return this._nom;
        },
        set: function (value) {
            this._nom = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SuperHero.prototype, "slipAuDessusDeSesVetements", {
        get: function () {
            return this._slipAuDessusDeSesVetements;
        },
        set: function (value) {
            this._slipAuDessusDeSesVetements = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SuperHero.prototype, "cape", {
        get: function () {
            return this._cape;
        },
        set: function (value) {
            this._cape = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(SuperHero.prototype, "force", {
        get: function () {
            return this._force;
        },
        set: function (value) {
            this._force = value;
        },
        enumerable: false,
        configurable: true
    });
    SuperHero.prototype.laBagarre = function () {
        console.log("BAM BIM TINDIN TOUDOUM l'appel");
    };
    return SuperHero;
}());
exports.SuperHero = SuperHero;
