import {Combat} from "./Combat";

export class SuperHero implements Combat {
    private _nom: String;
    private _slipAuDessusDeSesVetements: boolean;
    private _cape: boolean;
    private _force: number;

    constructor(nom: String, slipAuDessusDeSesVetements: boolean, cape: boolean = true, force?: number) {
        this._nom = nom;
        this._slipAuDessusDeSesVetements = slipAuDessusDeSesVetements;
        this._cape = cape;
        this._force = force;
    }

    get nom(): String {
        return this._nom;
    }

    set nom(value: String) {
        this._nom = value;
    }

    get slipAuDessusDeSesVetements(): boolean {
        return this._slipAuDessusDeSesVetements;
    }

    set slipAuDessusDeSesVetements(value: boolean) {
        this._slipAuDessusDeSesVetements = value;
    }

    get cape(): boolean {
        return this._cape;
    }

    set cape(value: boolean) {
        this._cape = value;
    }

    get force(): number {
        return this._force;
    }

    set force(value: number) {
        this._force = value;
    }

    public laBagarre(): void {
        console.log("BAM BIM TINDIN TOUDOUM l'appel");
    }
}