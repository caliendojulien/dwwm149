document.getElementById('btn').addEventListener(
    'click',
    clicSurLeBouton
);

function clicSurLeBouton() {
    const prenom = document.getElementById('prenom').value;
    fetch('https://api.agify.io/?name='+prenom+'&country_id=FR')
        .then(data => data.json())
        .then(json => {
            let age = json.age;
            let divAge = document.createElement('div');
            divAge.innerText = "Age : " + age;
            document.getElementById('resultat').appendChild(divAge);
        })
        .catch(erreur => {
            console.log(erreur);
            document.getElementById('resultat').innerText = '';
            document.getElementById('resultat').innerText = erreur;
        })
    fetch('https://api.genderize.io/?name='+prenom)
        .then(data => data.json())
        .then(json => {
            let genre = json.gender;
            let divGenre = document.createElement('div');
            divGenre.innerText = "Genre : " + genre;
            document.getElementById('resultat').appendChild(divGenre);
        })
    fetch('https://api.nationalize.io/?name='+prenom)
        .then(data => data.json())
        .then(json => {
            let nationalite = json.country[0].country_id;
            let divNationalite = document.createElement('div');
            divNationalite.innerText = "Nationalité : " + nationalite;
            document.getElementById('resultat').appendChild(divNationalite);
        })
}